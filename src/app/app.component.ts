import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data.service';

declare var $;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  //name var from validation
  nombre1: string;
  ocacion: string;

  //var from boolean validation
  valor_ocacion:boolean = false;

  dataArray:any=[];
  id='';
  nombre = '';
  constructor(public data: DataService){
    console.log("Hello fellow user");

    this.LoadTableData();

    setTimeout(function(){
      $(function(){
          $('#example').DataTable();
        });
    },3000);

  }

  search(term: string) {
    //si el valor ingresado por el usuario es nula arroja la excepcion
    if(term.length == 0){
    console.log("valor vacio");
      this.valor_ocacion = true;
    }
    //de lo contrario continua normalmente
    else{
    console.log("valor ");
      this.valor_ocacion = false;
    }

    }

console_prueba(id:string,nombre: string ){
  this.id = id;
  this.nombre = nombre;
  console.log("desde la funcion "+this.nombre);
}


function_add(term: string): void{
  this.ocacion=term;
alert("desde ADD"+ this.ocacion);
}
  ngOnInit(): void {}


  LoadTableData(){
    this.data.LoadData().subscribe(
      data=>{
        this.dataArray = data;
        console.log(data);
      }
    );
  }
}
